/*
 * @Author: leox_tian
 * @Date: 2021-10-20 13:56:25
 * @Description: file content
 */
#include <iostream>
#include <chrono>
#include <gtest/gtest.h>
#include "dbscan.h"
using namespace std;

TEST(testcase, test_dbscan)
{
    vector<Eigen::Vector2d> points(10);
    vector<vector<Eigen::Vector2d> > result;

    points[0].x() = 20; points[0].y() = 21;
    points[1].x() = 20; points[1].y() = 25;
    points[2].x() = 28; points[2].y() = 22;
    points[3].x() = 30; points[3].y() = 52;
    points[4].x() = 26; points[4].y() = 70;
    points[5].x() = 30; points[5].y() = 75;
    points[6].x() = 0;  points[6].y() = 70;
    points[7].x() = 70; points[7].y() = 50;
    points[8].x() = 67; points[8].y() = 69;
    points[9].x() = 80; points[9].y() = 35;

    result.resize(4);
    result[0].emplace_back(points[6]);
    result[1].emplace_back(points[0]);
    result[1].emplace_back(points[1]);
    result[1].emplace_back(points[2]);
    result[2].emplace_back(points[3]);
    result[2].emplace_back(points[4]);
    result[2].emplace_back(points[5]);
    result[3].emplace_back(points[7]);
    result[3].emplace_back(points[8]);
    result[3].emplace_back(points[9]);

    DBScan<Eigen::Vector2d> dbscan(20.0, 3);
    vector<vector<Eigen::Vector2d>> res = dbscan.GetClusters(points);

    cout << "noise " << endl;
    for(auto& r : res[0])
        cout << r.transpose() << endl;
    for (size_t i = 1; i < res.size(); i++)
    {
        cout << "cluster " << i << endl;
        for (size_t j = 0; j < res[i].size(); j++)
        {
            cout << res[i][j].transpose() << endl;
        }

    }

    EXPECT_EQ(result, res);
}

int main(int argc, char** argv) {

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}